package it.esteco.tdd;

/**
 * COPYRIGHT (C) 2015 ESTECO SpA. All Rights Reserved.
 * Created Oct 01, 2015 10:03 AM.
 */
public class Horse {

    private static final String CALL = "Hiii";

    public String neigh() {
        return CALL;
    }

    public boolean jump(Fence fence) {
        return fence.getHeight() <= 1;
    }
}
