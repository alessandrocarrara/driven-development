package it.esteco.tdd;

/**
 * COPYRIGHT (C) 2015 ESTECO SpA. All Rights Reserved.
 * Created Oct 01, 2015 10:21 AM.
 */
public class Fence {
    private static final String NEGATIVE_HEIGHT_MESSAGE = "The height is negative";
    private final int height;

    public Fence(int height) throws Exception {
        if (height < 0)
            throw new Exception(NEGATIVE_HEIGHT_MESSAGE);
        else
            this.height = height;
    }

    public int getHeight() {
        return height;
    }
}
