package it.esteco.tdd;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * COPYRIGHT (C) 2015 ESTECO SpA. All Rights Reserved.
 * Created Oct 01, 2015 10:03 AM.
 */
public class HorseTest {

    private Horse horse;

    @Before
    public void setUp() throws Exception {
        horse = new Horse();
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void shouldNeigh() {
        String expected = "Hiii";
        String actual = horse.neigh();
        assertEquals("It didn't neigh correctly", expected, actual);
    }

    @Test
    public void shouldJumpWhenEncountersA1MeterFence() throws Exception {
        Fence fence = new Fence(1);
        boolean actual = horse.jump(fence);
        assertTrue(actual);
    }

    @Test
    public void shouldJumpWhenEncountersA3MeterFence() throws Exception {
        Fence fence = new Fence(3);
        boolean actual = horse.jump(fence);
        assertFalse(actual);
    }
}