package it.esteco.tdd;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

/**
 * COPYRIGHT (C) 2015 ESTECO SpA. All Rights Reserved.
 * Created Oct 01, 2015 10:31 AM.
 */
public class FenceTest {

    @Test
    public void shouldThrowAnExceptionWhenCreatingAFenceWithNegativeHeight() {
        int height = -1;
        try {
            Fence fence = new Fence(height);
            fail();
        } catch (Exception e) {
            String expected = "The height is negative";
            String actual = e.getMessage();
            assertEquals(expected, actual);
        }
    }
}